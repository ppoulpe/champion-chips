<?php

namespace App\Infrastructure\Persistence;

use App\Core\Port\Persistence\PersistenceServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class PersistenceService implements PersistenceServiceInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $entity
     * @param bool $flush
     * @return bool
     */
    public function upsert($entity, bool $flush = true) : bool
    {
        try {
            $this->entityManager->persist($entity);

            if ($flush) {
                $this->entityManager->flush();
            }

            return true;
        } catch (\Exception $exception) {
            return false;
        }

    }

}