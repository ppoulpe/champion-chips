<?php

namespace App\UserInterface\Api\Rest\UseCase;

use App\Core\Component\Point\Repository\PointRepository;
use App\Core\Component\Point\Service\PointService;
use App\Core\Component\User\Service\UserService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Prefix("api")
 */
class UserController extends AbstractFOSRestController
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var PointService
     */
    private $pointService;

    /**
     * UserController constructor.
     * @param UserService $userService
     * @param PointService $pointService
     */
    public function __construct(
        UserService $userService,
        PointService $pointService
    )
    {
        $this->userService = $userService;
        $this->pointService = $pointService;
    }

    /**
     * @return Response
     */
    public function getUsersAction(): Response
    {
        return $this->handleView(
            $this->view(
                $this->userService->findUsers(),
                Response::HTTP_OK
            )
        );
    }

    /**
     * @param string $username
     * @return Response
     */
    public function getUserAction(string $username = null): Response
    {
        return $this->handleView(
            $this->view(
                $this->userService->findUser($username),
                Response::HTTP_OK
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postUsersAction(Request $request): Response
    {
        $userData = json_decode(
            $request->getContent(),
            true
        );

        return $this->handleView(
            $this->view(
                $this->userService->addUser($userData['username'], $userData['password']),
                Response::HTTP_OK
            )
        );
    }

    /**
     * @param string $username
     * @return Response
     */
    public function getUserPointsAction(string $username): Response
    {
        return $this->handleView(
            $this->view(
                $this->pointService->findAllPointsByUser($username),
                Response::HTTP_OK
            )
        );
    }

    /**
     * @param string $username
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function postUserPointsAction(string $username, Request $request): Response
    {
        $pointData = json_decode(
            $request->getContent(),
            true
        );

        return $this->handleView(
            $this->view(
                $this->pointService->addPoint(
                    $pointData['value'],
                    $pointData['comment'],
                    $username,
                    $pointData['type'] ?? PointRepository::POSITIVE_TYPE
                ),
                Response::HTTP_OK
            )
        );
    }
}