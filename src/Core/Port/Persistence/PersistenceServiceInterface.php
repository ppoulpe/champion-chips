<?php

namespace App\Core\Port\Persistence;

interface PersistenceServiceInterface
{
    public function upsert($entity, bool $flush = true) : bool;
}
