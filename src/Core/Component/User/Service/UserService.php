<?php

namespace App\Core\Component\User\Service;

use App\Core\Component\User\Domain\User\User;
use App\Core\Component\User\Exception\InvalidUserException;
use App\Core\Component\User\Exception\UserNotFoundException;
use App\Core\Component\User\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param Security $security
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ValidatorInterface $validator
     */
    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        Security $security,
        UserPasswordEncoderInterface $passwordEncoder,
        ValidatorInterface $validator
    )
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->security = $security;
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public function findUsers(): array
    {
        try {
            return $this->userRepository->getUsers();
        } catch (UserNotFoundException $userNotFoundException) {
            return [
                'success' => false,
                'code' => $userNotFoundException->getCode(),
                'message' => $userNotFoundException->getMessage()
            ];
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'message' => 'Une erreur inconnue est survenue.'
            ];
        }
    }

    /**
     * @param string|null $username
     * @return array
     */
    public function findUser(string $username = null): array
    {
        try {
            return $this->userRepository->getUserByUsername(
                $username ?? $this->security->getToken()->getUsername()
            );
        } catch (UserNotFoundException $notFoundException) {
            return [
                'success' => false,
                'code' => $notFoundException->getCode(),
                'message' => $notFoundException->getMessage()
            ];
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'message' => 'Une erreur inconnue est survenue.'
            ];
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @return array
     */
    public function addUser(
        string $username,
        string $password
    ): array
    {
        try {

            $user = (new User())
                ->setUsername($username)
                ->setRoles([$this->userRepository::ROLE_USER]);

            $user->setPassword(
                $this->passwordEncoder->encodePassword($user, $password)
            );

            $this->checkUser($user);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return [
                'success' => true,
                'user' => $user
            ];

        } catch (InvalidUserException $invalidUserException) {
            return [
                'success' => false,
                'code' => $invalidUserException->getCode(),
                'message' => $invalidUserException->getMessage()
            ];
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'message' => 'Une erreur inconnue est survenue.'
            ];
        }
    }

    /**
     * @param User $user
     * @return bool
     * @throws InvalidUserException
     */
    private function checkUser(User $user): bool
    {
        $errors = $this->validator->validate($user);

        if (!empty($errors->count())) {
            throw new InvalidUserException((string)$errors);
        }

        return true;
    }

}