<?php

namespace App\Core\Component\User\Repository;

use App\Core\Component\Point\Domain\Point\Point;
use App\Core\Component\Point\Repository\PointRepository;
use App\Core\Component\User\Domain\User\User;
use App\Core\Component\User\Exception\UserNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    const ROLE_USER = "ROLE_USER";
    const ROLE_ADMIN = "ROLE_ADMIN";

    /**
     * UserRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return array|null
     * @throws UserNotFoundException
     */
    public function getUsers(): ?array
    {
        $return = [];
        $resources = $this->findAll();

        if (empty($resources)) {
            throw new UserNotFoundException();
        }

        foreach ($resources as $resource) {
            $return[] = $this->formatResult($resource);
        }

        return $return;
    }

    /**
     * @param string $username
     * @return array|null
     * @throws UserNotFoundException
     */
    public function getUserByUsername(string $username): ?array
    {
        $resource = $this->findOneBy([
            "username" => $username
        ]);

        if (empty($resource)) {
            throw new UserNotFoundException();
        }

        return $this->formatResult($resource);
    }

    /**
     * Calculate user's score and format output result
     * @param User $user
     * @return array
     */
    private function formatResult(User $user): array
    {
        return [
            'username' => $user->getUsername(),
            'score' => $this->calculateScore($user)
        ];
    }

    /**
     * @param User $user
     * @return int
     */
    private function calculateScore(User $user): int
    {
        $score = 0;

        $user->getPoints()->map(function (Point $point) use (&$score) {

            $score +=
                ($point->getType() == PointRepository::POSITIVE_TYPE) ?
                    $point->getValue() :
                    -$point->getValue();
        });

        return $score;
    }
}
