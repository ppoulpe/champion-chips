<?php

namespace App\Core\Component\User\Exception;

use Throwable;

class InvalidUserException extends \Exception
{
    const MESSAGE = "L'utilisateur n'est pas valide.";
    const CODE = 21500;

    /**
     * SameRecipientAndSenderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = self::MESSAGE,
        $code = self::CODE,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}