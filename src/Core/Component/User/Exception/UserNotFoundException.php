<?php

namespace App\Core\Component\User\Exception;

use Throwable;

class UserNotFoundException extends \Exception
{
    const MESSAGE = "Aucun utilisateur n'a été trouvé.";
    const CODE = 21404;

    /**
     * SameRecipientAndSenderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = self::MESSAGE,
        $code = self::CODE,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}