<?php

namespace App\Core\Component\User\ValueObject;

class Point
{
    /**
     * @var int $value
     */
    private $value;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $comment
     */
    private $comment;

    /**
     * @var string $from
     */
    private $from;

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return Point
     */
    public function setValue(int $value): Point
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Point
     */
    public function setType(string $type): Point
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return Point
     */
    public function setComment(string $comment): Point
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     * @return Point
     */
    public function setFrom(string $from): Point
    {
        $this->from = $from;
        return $this;
    }



}