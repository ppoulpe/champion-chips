<?php

namespace App\Core\Component\User\Domain\User;

use App\Core\Component\Point\Domain\Point\Point;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Core\Component\User\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     min = 3,
     *     max = 180,
     *     minMessage = "Le nom d'utilisateur doit comporter au minimum {{ limit }} caractères.",
     *     maxMessage = "Le nom d'utilisateur doit comporter au maximum {{ limit }} caractères."
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Core\Component\Point\Domain\Point\Point", mappedBy="toUser", orphanRemoval=true)
     */
    private $points;

    /**
     * @ORM\OneToMany(targetEntity="App\Core\Component\Point\Domain\Point\Point", mappedBy="fromUser", orphanRemoval=true)
     */
    private $toPoints;

    public function __construct()
    {
        $this->points = new ArrayCollection();
        $this->toPoints = new ArrayCollection();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Point[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    public function addPoint(Point $point): self
    {
        if (!$this->points->contains($point)) {
            $this->points[] = $point;
            $point->setToUser($this);
        }

        return $this;
    }

    public function removePoint(Point $point): self
    {
        if ($this->points->contains($point)) {
            $this->points->removeElement($point);
            // set the owning side to null (unless already changed)
            if ($point->getToUser() === $this) {
                $point->setToUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Point[]
     */
    public function getToPoints(): Collection
    {
        return $this->toPoints;
    }

    public function addToPoint(Point $toPoint): self
    {
        if (!$this->toPoints->contains($toPoint)) {
            $this->toPoints[] = $toPoint;
            $toPoint->setFromUser($this);
        }

        return $this;
    }

    public function removeToPoint(Point $toPoint): self
    {
        if ($this->toPoints->contains($toPoint)) {
            $this->toPoints->removeElement($toPoint);
            // set the owning side to null (unless already changed)
            if ($toPoint->getFromUser() === $this) {
                $toPoint->setFromUser(null);
            }
        }

        return $this;
    }
}
