<?php

namespace App\Core\Component\Point\Repository;

use App\Core\Component\Point\Domain\Point\Point;
use App\Core\Port\Persistence\PersistenceServiceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Point|null find($id, $lockMode = null, $lockVersion = null)
 * @method Point|null findOneBy(array $criteria, array $orderBy = null)
 * @method Point[]    findAll()
 * @method Point[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PointRepository extends ServiceEntityRepository
{
    const POSITIVE_TYPE = "POSITIVE";
    const NEGATIVE_TYPE = "NEGATIVE";
    /**
     * @var PersistenceServiceInterface
     */
    private $persistenceService;

    public function __construct(
        RegistryInterface $registry,
        PersistenceServiceInterface $persistenceService
    )
    {
        parent::__construct($registry, Point::class);
        $this->persistenceService = $persistenceService;
    }

    /**
     * @param Point $point
     * @return bool
     */
    public function insert(Point $point) : bool
    {
        return $this->persistenceService->upsert($point);
    }
}
