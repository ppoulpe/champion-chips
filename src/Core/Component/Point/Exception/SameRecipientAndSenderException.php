<?php

namespace App\Core\Component\Point\Exception;

use Throwable;

class SameRecipientAndSenderException extends \Exception
{
    const MESSAGE = "L'emetteur et le destinataire ne peuvent pas être la même personne.";
    const CODE = 16500;

    /**
     * SameRecipientAndSenderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = self::MESSAGE,
        $code = self::CODE,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}