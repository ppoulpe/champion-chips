<?php

namespace App\Core\Component\Point\Rule;

use App\Core\Component\Point\Exception\SameRecipientAndSenderException;

class SameRecipientAndSenderRule extends Rule implements RuleInterface
{
    /**
     * @return bool
     * @throws SameRecipientAndSenderException
     */
    public function verify(): bool
    {
        if ($this->getPoint()->getFromUser() === $this->getPoint()->getToUser()) {
            throw new SameRecipientAndSenderException();
        }

        return true;
    }

}