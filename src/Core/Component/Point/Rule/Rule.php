<?php

namespace App\Core\Component\Point\Rule;

use App\Core\Component\Point\Domain\Point\Point;

class Rule
{
    /**
     * @var Point
     */
    private $point;

    /**
     * Rule constructor.
     * @param Point $point
     */
    public function __construct(Point $point)
    {
        $this->point = $point;
    }

    /**
     * @return Point
     */
    public function getPoint(): Point
    {
        return $this->point;
    }

    /**
     * @param Point $point
     * @return Rule
     */
    public function setPoint(Point $point): Rule
    {
        $this->point = $point;
        return $this;
    }


}