<?php

namespace App\Core\Component\Point\Rule;

interface RuleInterface
{
    public function verify() : bool;
}