<?php

namespace App\Core\Component\Point\Service;

use App\Core\Component\Point\Domain\Point\Point;
use App\Core\Component\Point\Rule\RuleInterface;
use Doctrine\Common\Collections\ArrayCollection;

class RuleService
{
    /**
     * @var ArrayCollection $rules
     */
    private $rules;

    /**
     * RuleService constructor.
     * @param RuleInterface ...$rules
     */
    public function __construct(RuleInterface ...$rules)
    {
        $this->rules = new ArrayCollection($rules);
    }

    /**
     * @param Point $point
     * @return ArrayCollection
     */
    public function verifyAllRules(Point $point) : ArrayCollection
    {
        $errors = new ArrayCollection();

        $this->rules->map(function(RuleInterface $rule) use(&$errors, $point){
            try{
                $rule
                    ->setPoint($point)
                    ->verify();
            }catch (\Exception $exception){
                $errors->add([
                    "code" => $exception->getCode(),
                    "message" => $exception->getMessage()
                ]);
            }
        });

        return $errors;
    }
}