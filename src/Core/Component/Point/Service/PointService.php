<?php

namespace App\Core\Component\Point\Service;

use App\Core\Component\Point\Domain\Point\Point;
use App\Core\Component\Point\Repository\PointRepository;
use App\Core\Component\User\Repository\UserRepository;
use App\Core\Component\User\ValueObject\Point as UserPoint;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Security;

class PointService
{
    /**
     * @var PointRepository
     */
    private $pointRepository;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RuleService
     */
    private $ruleService;

    /**
     * PointService constructor.
     * @param PointRepository $pointRepository
     * @param Security $security
     * @param UserRepository $userRepository
     * @param RuleService $ruleService
     */
    public function __construct(
        PointRepository $pointRepository,
        Security $security,
        UserRepository $userRepository,
        RuleService $ruleService
    )
    {
        $this->pointRepository = $pointRepository;
        $this->security = $security;
        $this->userRepository = $userRepository;
        $this->ruleService = $ruleService;
    }

    /**
     * @param string $username
     * @return ArrayCollection
     */
    public function findAllPointsByUser(string $username): ArrayCollection
    {
        $points = new ArrayCollection();

        $resource = $this->pointRepository->findBy(
            ['toUser' => $this->userRepository->findOneBy(['username' => $username])]
        );

        foreach ($resource as $point) {
            $points->add(
                (new UserPoint())
                    ->setValue($point->getValue())
                    ->setType($point->getType())
                    ->setComment($point->getComment())
                    ->setFrom($point->getFromUser()->getUsername())
            );
        }

        return $points;
    }

    /**
     * @param int $value
     * @param string $comment
     * @param string $username
     * @param string $type
     * @return array
     * @throws Exception
     */
    public function addPoint(
        int $value,
        string $comment,
        string $username,
        string $type
    ): array
    {
        $point = (new Point())
            ->setValue($value)
            ->setComment($comment)
            ->setFromUser($this->userRepository->find($this->security->getUser()))
            ->setToUser($this->userRepository->findOneBy(['username' => $username]))
            ->setType($type)
            ->setCreatedAt(new DateTime());

        $checks = $this->ruleService->verifyAllRules($point);

        return $checks->count() ?
            [
                "success" => false,
                "errors" => $checks->toArray()
            ] :
            [
                "success" => $this->pointRepository->insert($point)
            ];
    }
}