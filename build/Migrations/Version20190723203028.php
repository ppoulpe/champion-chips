<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190723203028 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE point ADD CONSTRAINT FK_B7A5F3242130303A FOREIGN KEY (from_user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B7A5F3242130303A ON point (from_user_id)');
        $this->addSql('ALTER TABLE point RENAME INDEX fk_b7a5f32429f6ee60 TO IDX_B7A5F32429F6EE60');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE point DROP FOREIGN KEY FK_B7A5F3242130303A');
        $this->addSql('DROP INDEX IDX_B7A5F3242130303A ON point');
        $this->addSql('ALTER TABLE point RENAME INDEX idx_b7a5f32429f6ee60 TO FK_B7A5F32429F6EE60');
    }
}
