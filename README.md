# Démarrage rapide

Démarrer Docker puis lancer les containers avec la commande :

```
docker-compose up -d
```

Se rendre dans un bash du container PHP avec la commande :
```
docker exec -it cc_php bash
```

Installer les dépendances composer et executer les requêtes SQL pour mettre à jour la base
```
composer install
php bin/console doctrine:migrations:migrate
```

# Utilisation

La collection Postman se trouve ici : https://www.getpostman.com/collections/db96b37e51bcef9105ef
